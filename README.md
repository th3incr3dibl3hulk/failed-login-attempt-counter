# Failed Login Attempts Summary Script

This script reads the authentication log file (auth.log) located at /var/log/auth.log and provides a summary of failed login attempts by IP address. It uses regular expressions to parse the log file and extract the necessary information.

## Prerequisites
Python 3.x    
Access to the /var/log/auth.log file (usually requires root or sudo privileges)

## Usage
Save the script to a file: Save the provided Python script to a file, for example, failed_login_summary.py.

Run the script:
    bash

sudo python3 failed_login_summary.py

Note: Running the script with sudo is typically necessary to access the auth.log file due to permission restrictions.

## Output
The script outputs a summary of failed login attempts by IP address in the following format:


Summary of failed login attempts by IP address:
---------------------------------------------
IP Address: <IP_ADDRESS_1> - Attempts: <COUNT_1>
IP Address: <IP_ADDRESS_2> - Attempts: <COUNT_2>
...
---------------------------------------------


## Notes
Ensure that the script has appropriate permissions to read the /var/log/auth.log file.
The script only considers lines containing "Failed password" and extracts the first IPv4 address found in those lines.
Adjust the script if your log format or requirements differ.