import re

# Define the path to the auth log file
AUTH_LOG_FILE = "/var/log/auth.log"

# Open the auth log file for reading
with open(AUTH_LOG_FILE, "r") as f:
    # Initialize empty dictionary to store IP and attempt count
    ip_attempt_count = {}

    # Read each line of the log file
    for line in f:
        # Use regex to match lines containing "Failed password"
        if re.search(r"Failed password", line):
            # Extract IP address from the line
            ip_address = re.findall(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", line)[0]

            # Check if IP address already exists in the dictionary
            if ip_address in ip_attempt_count:
                # Increment the existing count
                ip_attempt_count[ip_address] += 1
            else:
                # Add new entry to the dictionary with count 1
                ip_attempt_count[ip_address] = 1

# Print summary of failed attempts
print("Summary of failed login attempts by IP address:")
print("---------------------------------------------")

# Loop through each IP address and corresponding attempt count
for ip_address, attempt_count in ip_attempt_count.items():
    print(f"IP Address: {ip_address} - Attempts: {attempt_count}")

print("---------------------------------------------")
